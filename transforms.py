from images import *
from images import read_img, write_img, create_blank, size


def mirror(image: dict) -> dict:
    (width, height) = size(image)
    image_mirrored = create_blank(width, height)
    for fila in range(height):
        for columna in range(width):
            image_mirrored['pixels'][fila*width+columna] = image['pixels'][(height-1-fila)*width+columna]

    return image_mirrored

def grayscale(image: dict) -> dict:

    (width, height) = size(image)
    image_grayscale = create_blank(width, height)
    for i in range(width*height):
        r = image['pixels'][i][0]# color rojo del pixel i de la imagen
        g = image["pixels"][i][1]
        b = image["pixels"][i][2]
        C = int((r + g + b) / 3)
        image_grayscale['pixels'][i] = (C, C, C)

    return image_grayscale

def blur(image: dict) -> dict:

    (width, height) = size(image)
    image_blur = create_blank(width, height)
    for x in range(width):
        for y in range(height):
            if y == 0:
                if x == 0:
                    R = int((image['pixels'][x + 1 + y * width][0] + image['pixels'][x + (y + 1) * width][0]) / 2)
                    G = int((image['pixels'][x + 1 + y * width][1] + image['pixels'][x + (y + 1) * width][1]) / 2)
                    B = int((image['pixels'][x + 1 + y * width][2] + image['pixels'][x + (y + 1) * width][2]) / 2)
                elif x == width - 1:
                    R = int((image['pixels'][x - 1 + y * width][0] + image['pixels'][x + (y + 1) * width][0]) / 2)
                    G = int((image['pixels'][x - 1 + y * width][1] + image['pixels'][x + (y + 1) * width][1]) / 2)
                    B = int((image['pixels'][x - 1 + y * width][2] + image['pixels'][x + (y + 1) * width][2]) / 2)
                else:
                    R = int((image['pixels'][x - 1 + y * width][0] + image['pixels'][x + 1 + y * width][0] +
                             image['pixels'][x + (y + 1) * width][0]) / 3)
                    G = int((image['pixels'][x - 1 + y * width][1] + image['pixels'][x + 1 + y * width][1] +
                             image['pixels'][x + (y + 1) * width][1]) / 3)
                    B = int((image['pixels'][x - 1 + y * width][2] + image['pixels'][x + 1 + y * width][2] +
                             image['pixels'][x + (y + 1) * width][2]) / 3)

            elif y == (height - 1):
                if x == 0:
                    R = int((image['pixels'][x + 1 + y * width][0] + image['pixels'][x + (y - 1) * width][0]) / 2)
                    G = int((image['pixels'][x + 1 + y * width][1] + image['pixels'][x + (y - 1) * width][1]) / 2)
                    B = int((image['pixels'][x + 1 + y * width][2] + image['pixels'][x + (y - 1) * width][2]) / 2)
                elif x == width - 1:
                    R = int((image['pixels'][x - 1 + y * width][0] + image['pixels'][x + (y - 1) * width][0]) / 2)
                    G = int((image['pixels'][x - 1 + y * width][1] + image['pixels'][x + (y - 1) * width][1]) / 2)
                    B = int((image['pixels'][x - 1 + y * width][2] + image['pixels'][x + (y - 1) * width][2]) / 2)
                else:
                    R = int((image['pixels'][x - 1 + y * width][0] + image['pixels'][x + 1 + y * width][0] +
                             image['pixels'][x + (y - 1) * width][0]) / 3)
                    G = int((image['pixels'][x - 1 + y * width][1] + image['pixels'][x + 1 + y * width][1] +
                             image['pixels'][x + (y - 1) * width][1]) / 3)
                    B = int((image['pixels'][x - 1 + y * width][2] + image['pixels'][x + 1 + y * width][2] +
                             image['pixels'][x + (y - 1) * width][2]) / 3)
            else:
                if x == 0:
                    R = int((image['pixels'][x + 1 + y * width][0] + image['pixels'][x + (y + 1) * width][0] +
                             image['pixels'][x + (y + 1) * width][0]) / 3)
                    G = int((image['pixels'][x + 1 + y * width][1] + image['pixels'][x + (y + 1) * width][1] +
                             image['pixels'][x + (y + 1) * width][1]) / 3)
                    B = int((image['pixels'][x + 1 + y * width][2] + image['pixels'][x + (y + 1) * width][2] +
                             image['pixels'][x + (y + 1) * width][2]) / 3)
                elif x == width - 1:
                    R = int((image['pixels'][x - 1 + y * width][0] + image['pixels'][x + (y + 1) * width][0] +
                             image['pixels'][x + (y + 1) * width][0]) / 3)
                    G = int((image['pixels'][x - 1 + y * width][1] + image['pixels'][x + (y + 1) * width][1] +
                             image['pixels'][x + (y + 1) * width][1]) / 3)
                    B = int((image['pixels'][x - 1 + y * width][2] + image['pixels'][x + (y + 1) * width][2] +
                             image['pixels'][x + (y + 1) * width][2]) / 3)

                else:
                    R = int((image['pixels'][x - 1 + y * width][0] + image['pixels'][x + 1 + y * width][0] +
                             image['pixels'][x + (y + 1) * width][0] + image['pixels'][x + (y + 1) * width][0]) / 4)
                    G = int((image['pixels'][x - 1 + y * width][1] + image['pixels'][x + 1 + y * width][1] +
                             image['pixels'][x + (y + 1) * width][1] + image['pixels'][x + (y + 1) * width][1]) / 4)
                    B = int((image['pixels'][x - 1 + y * width][2] + image['pixels'][x + 1 + y * width][2] +
                             image['pixels'][x + (y + 1) * width][2] + image['pixels'][x + (y + 1) * width][1]) / 4)

            image_blur['pixels'][y*width+x] = (R, G, B)

    return image_blur

def change_colors(image: dict,
                 original: list[tuple[int, int, int]],
                 change: list[tuple[int, int, int]]) -> dict:

    (width, height) = size(image)
    image_changed_color = create_blank(width, height)
    for fila in range(height):
        for columna in range(width):
            image_changed_color['pixels'][fila * width + columna] = image['pixels'][fila * width + columna]
            for i in range(len(original)):
                if image_changed_color['pixels'][fila * width + columna] == original[i]:
                    image_changed_color["pixels"][fila * width + columna] = change[i]

    return image_changed_color

#he hecho un cambio a la función en si
def rotate(image: dict, direction: str) -> dict:

    (width, height) = size(image)
    image_rotated = create_blank(height, width)
    while(direction != "left" and direction != "right"):
        direction = input("El tipo de rotación es: (left or right). Introduce la rotación deseada: ")
    for fila in range(width):
        for columna in range(height):
            if (direction == "right"):
                image_rotated['pixels'][fila * height + columna] = image['pixels'][(height - 1 - columna) * width + fila] #giro a la derecha

            elif (direction == "left"):
                image_rotated['pixels'][fila * height + columna] = image['pixels'][columna * width + (width - 1 - fila)] #giro a la izquierda

    return image_rotated

def shift(image: dict, horizontal: int = 0, vertical: int = 0) -> dict:
    (width, height) = size(image)
    image_shift = create_blank(width+horizontal, height+vertical)
    for fila in range(height+vertical):
        for columna in range(width+horizontal):
            if (fila < vertical or columna < horizontal):
                image_shift['pixels'][fila*(width+horizontal)+columna] = (0, 0, 0)
            else:
                image_shift["pixels"][fila*(width+horizontal)+columna] = image["pixels"][(fila-vertical)*width+(columna-horizontal)]

    return image_shift

def crop(image: dict, x: int, y: int, width: int,height: int) -> dict:

    (width_image, height_image) = size(image)
    image_crop = create_blank(width, height)

    for fila in range(width):
        for columna in range(height):
            image_crop["pixels"][fila*width+columna] = image["pixels"][(y + fila)*width_image+(x+columna)]
    return image_crop

def filter(image: dict, r: float, g: float, b: float) -> dict:

    (width, height) = size(image)
    image_filtered = create_blank(width, height)
    for fila in range(height):
        for columna in range(width):
            R = min(int(image["pixels"][fila*width+columna][0] * r), 255)
            G = min(int(image["pixels"][fila*width+columna][1] * g), 255)
            B = min(int(image["pixels"][fila*width+columna][2] * b), 255)

            image_filtered['pixels'][fila*width+columna] = (R, G, B)

    return image_filtered

# filtro extra
def sepia(image: dict) -> dict:

    (width, height) = size(image)
    image_sepia = create_blank(width, height)
    for i in range(width*height):
        r = image['pixels'][i][0]
        g = image['pixels'][i][1]
        b = image['pixels'][i][2]
        r_sepia = int(0.393 * r + 0.769 * g + 0.189 * b)
        g_sepia = int(0.349 * r + 0.686 * g + 0.168 * b)
        b_sepia = int(0.272 * r + 0.534 * g + 0.131 * b)
        image_sepia['pixels'][i] = (r_sepia, g_sepia, b_sepia)

    return image_sepia

