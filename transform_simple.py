import sys
from transforms import mirror, blur, grayscale, sepia
from images import read_img, write_img, create_blank

#hemos añadido el filtro sepia
def main():
    image = read_img(sys.argv[1])

    if sys.argv[2] == "mirror":
        image_trans = mirror(image)
    elif sys.argv[2] == "blur":
        image_trans = blur(image)
    elif sys.argv[2] == "grayscale":
        image_trans = grayscale(image)
    elif sys.argv[2] == "sepia":
        image_trans = sepia(image)
    else:
        print("transformación no válida")

    file_name = sys.argv[1].replace(".", "_trans.")
    write_img(image_trans, file_name)

if __name__ == '__main__':
    main()