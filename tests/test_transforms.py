#!/usr/bin/python3

import unittest

from transforms import change_colors, mirror, grays

black = (0, 0, 0)
white = (255, 255, 255)
gray = (85, 85, 85)
red = (255, 0, 0)
green = (0, 255, 0)
blue = (0, 0, 255)

image_black = {'width': 3, 'height': 4, 'pixels': [black]*12}

image_white = {'width': 3, 'height': 4, 'pixels': [white]*12}

image_redblue = {'width': 3, 'height': 4,
                 'pixels': [red, red, red,
                            blue, blue, blue,
                            red, red, red,
                            blue, blue, blue]}

image_greenblue = {'width': 3, 'height': 4,
                 'pixels': [green, green, green,
                            blue, blue, blue,
                            green, green, green,
                            blue, blue, blue]}

image_bluered = {'width': 3, 'height': 4,
                  'pixels': [blue, blue, blue,
                             red, red, red,
                             blue, blue, blue,
                             red, red, red]}

image_greenblue_mirror = {'width': 3, 'height': 4,
                          'pixels': [blue, blue, blue,
                                     green, green, green,
                                     blue, blue, blue,
                                     green, green, green]}

image_greenblue_gray = {'width': 3, 'height': 4,
                          'pixels': [gray]*12}

image_redblue5 = {'width': 3, 'height': 5,
                 'pixels': [red, red, red,
                            blue, blue, blue,
                            red, red, red,
                            blue, blue, blue,
                            red, red, red]}

image_greenblue5 = {'width': 3, 'height': 5,
                 'pixels': [green, green, green,
                            blue, blue, blue,
                            green, green, green,
                            blue, blue, blue,
                            green, green, green]}

image_greengray5 = {'width': 3, 'height': 5,
                 'pixels': [green, green, green,
                            green, green, green,
                            green, green, green,
                            gray, gray, gray,
                            gray, gray, gray]}

image_greengray5_mirror = {'width': 3, 'height': 5,
                 'pixels': [gray, gray, gray,
                            gray, gray, gray,
                            green, green, green,
                            green, green, green,
                            green, green, green]}


class TestChangeColors(unittest.TestCase):

    def test_red_green(self):
        result = change_colors(image_redblue, original=[red], change=[green])
        self.assertEqual(result, image_greenblue)

    def test_black_white(self):
        result = change_colors(image_black, original=[black], change=[white])
        self.assertEqual(result, image_white)

    def test_two_colors(self):
        result = change_colors(image_redblue, original=[red, blue], change=[blue, red])
        self.assertEqual(result, image_bluered)

    def test_red_green5(self):
        result = change_colors(image_redblue5, original=[red], change=[green])
        self.assertEqual(result, image_greenblue5)

    def test_two_colors_void(self):
        result = change_colors(image_redblue5, original=[white, red], change=[black, green])
        self.assertEqual(result, image_greenblue5)


class TestMirror(unittest.TestCase):

    def test_greenblue(self):
        result = mirror(image_greenblue)
        self.assertEqual(result, image_greenblue_mirror)

    def test_greengray5(self):
        result = mirror(image_greengray5)
        self.assertEqual(result, image_greengray5_mirror)

class TestGray(unittest.TestCase):

    def test_greenblue(self):
        result = grayscale(image_greenblue)
        self.assertEqual(result, image_greenblue_gray)


if __name__ == '__main__':
    unittest.main()

