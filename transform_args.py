import sys
from transforms import change_colors, rotate, shift, crop, filter

from images import read_img, write_img, create_blank


def main():
    print(sys.argv[1])
    print(sys.argv[2])
    image = read_img(sys.argv[1])

    if sys.argv[2] == "change_colors":
        original = eval("[(" + sys.argv[3].replace(":","),(") + ")]")
        change = eval("[(" + sys.argv[4].replace(":", "),(") + ")]")
        image_trans = change_colors(image, original, change)
    elif sys.argv[2] == "rotate":
        image_trans = rotate(image, str(sys.argv[3]))
    elif sys.argv[2] == "shift":
        image_trans = shift(image, int(sys.argv[3]), int(sys.argv[4]))
    elif sys.argv[2] == "crop":
        image_trans = crop(image, int(sys.argv[3]), int(sys.argv[4]), int(sys.argv[5]), int(sys.argv[6]))
    elif sys.argv[2] == "filter":
        image_trans = filter(image, float(sys.argv[3]), float(sys.argv[4]), float(sys.argv[5]))
    else:
        print("transformación no válida")

    file_name = sys.argv[1].replace(".", "_trans.")
    write_img(image_trans, file_name)

if __name__ == '__main__':
    main()